package Sanity;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.apache.commons.mail.*;
import dataProvider.ConfigFileReader;
import pageFactory.BackofficePage;
import pageFactory.OnboardingPage;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Sanity.GetScreenShot;

public class CPSanityCheck {
	WebDriver driver;	
	WebDriverWait wait;
	ExtentReports extent;
    ExtentTest test;
    BackofficePage BackofcObj;
    OnboardingPage onboardingObj;
    ConfigFileReader configFileReader= new ConfigFileReader();
    	
	
	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", configFileReader.getDriverPath());
		extent = new ExtentReports(System.getProperty("user.dir") + configFileReader.getReportPath(), true);
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
		

	}
	
	@BeforeMethod
	public void beforeMethod(Method method)
	{
		
		test = extent.startTest(method.getName());
	}
	
	@AfterMethod
	
	public void afterMethod(ITestResult result) throws IOException  {

	    String methodName = result.getMethod().getMethodName();
        switch (result.getStatus()) {
	    
	    case ITestResult.SUCCESS:
	        {
	        	test.log(LogStatus.PASS, "PASSED: "+methodName);
	        	String screenShotPath = GetScreenShot.capture(driver);
	            test.log(LogStatus.PASS, "Snapshot below: " + test.addScreenCapture(screenShotPath));
	            break;
	        }
	        
	    case ITestResult.FAILURE:
	        {
	        	String screenShotPath = GetScreenShot.capture(driver);
                test.log(LogStatus.FAIL, "FAILED: "+methodName);
	      	  	test.log(LogStatus.FAIL, result.getThrowable());
                test.log(LogStatus.FAIL, "Snapshot below: " + test.addScreenCapture(screenShotPath));
	            break;
	        }
	    case ITestResult.SKIP:
	        {
	          	test.log(LogStatus.FAIL, "SKIPPED: "+methodName);
	            break;
	        }
	    default:
	        throw new RuntimeException("Invalid status");
	        
	    }
	    extent.endTest(test);
	    //driver.quit();
	}
	 
	
	
	@AfterClass
	public void tearDown() {
		driver.quit();
		extent.flush();
	    extent.close();
	}
	
	 //After complete execution send pdf report by email

	@AfterSuite
	   public void afterSuite(){
	  
	  sendReportByOutlook("priyanka@techmojo.in", "Sensistest@456",
	  "priyanka@techmojo.in", "Test Report", "");
	  
	  }
	
	  private static void sendReportByOutlook(final String from, final String pass, final String to,
	  final String subject, final String body) {
		  
		  Email email = new SimpleEmail();

		  email.setHostName("smtp.office365.com");
		  email.setSmtpPort(587);  // 993, 587, 465
		  email.setSocketTimeout(10000000);
		  email.setSSL(false);;
		  email.setAuthenticator(new DefaultAuthenticator(from, pass));
		  email.setTLS(true);// .setStartTLSEnabled(true);
		  try {
		      email.setFrom(from);
		      email.setSubject("subject text");
		      email.setDebug(false);
		      email.setMsg("This is a test mail ... :-)" );
		      email.addTo(to);
		      email.send();
		  } catch (EmailException e) {
		      //e.printStackTrace();
		  }
		  
		  /*//String portNumber = "465";
		  String portNumber = "587";
		  //String portNumber = "25";

			// Create object of Property file
			Properties props = new Properties();
	 
			// this will set host of server- you can change based on your requirement
			props.put("mail.smtp.host", "smtp.office365.com");
		    props.put("mail.smtp.starttls.enable","false");

	 
			// set the port of socket factory 
			props.put("mail.smtp.socketFactory.port", portNumber);
	 
			// set socket factory
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
	 
			// set the authentication to true
			props.put("mail.smtp.auth", "true");
	 
			// set the port of SMTP server
			props.put("mail.smtp.port", portNumber);
	 
			// This will handle the complete authentication
			Session session = Session.getDefaultInstance(props,
	 
					new javax.mail.Authenticator() {
	 
						protected PasswordAuthentication getPasswordAuthentication() {
	 
						return new PasswordAuthentication(from, pass);
	 
						}
	 
					});
			session.setDebug(true);
	 
			try {
	 
				// Create object of MimeMessage class
				Message message = new MimeMessage(session);
	 
				// Set the from address
				message.setFrom(new InternetAddress(from));
	 
				// Set the recipient address
				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(from));
	            
	                        // Add the subject link
				message.setSubject("Testing Subject");
	 
				// Create object to add multimedia type content
				BodyPart messageBodyPart1 = new MimeBodyPart();
	 
				// Set the body of email
				messageBodyPart1.setText("This is message body");
	 
				// Create another object to add another content
				MimeBodyPart messageBodyPart2 = new MimeBodyPart();
	 
				// Mention the file which you want to send
				String filename = "C:\\Users\\priyanka\\Workspace\\Sanity\\test-output\\emailable-report.html";//System.getProperty("user.dir")+"\\test-output\\emailable-report.html";
				System.out.println(filename + "------------------");

				// Create data source and pass the filename
				DataSource source = new FileDataSource(filename);
	 
				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(source));
	 
				// set the file
				messageBodyPart2.setFileName(filename);
	 
				// Create object of MimeMultipart class
				Multipart multipart = new MimeMultipart();
	 
				// add body part 1
				multipart.addBodyPart(messageBodyPart2);
	 
				// add body part 2
				multipart.addBodyPart(messageBodyPart1);
	 
				// set the content
				message.setContent(multipart);
	 
				System.out.println("=====Sending mail=====");
				System.out.println(message.toString());
				
				// finally send the email
				Transport.send(message);
	 
				System.out.println("=====Email Sent=====");
	 
			} catch (MessagingException e) {
				System.out.println("=====Email failed to send=====");
				System.out.println(e.toString());
				e.printStackTrace();
				System.out.println("======================");
				//throw new RuntimeException(e);
	 
			}
		  
		  /*
	  
	  Properties props = System.getProperties(); 
	  String host = "smtp.office365.com"; 
	  props.put("mail.smtp.starttls.enable", "false");
	  props.put("mail.smtp.host", host); 
	  props.put("mail.smtp.user", from);
	  props.put("mail.smtp.password", pass); 
	  props.put("mail.smtp.port", "587");
	  props.put("mail.smtp.auth", "true");
	  
	  Session session = Session.getDefaultInstance(props); MimeMessage message =
	  new MimeMessage(session); try {
	  
	  //Set from address
	  
	  message.setFrom(new InternetAddress(from));
	  message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	  //Set subject
	  
	  message.setSubject(subject); message.setText(body);
	  
	  BodyPart objMessageBodyPart = new MimeBodyPart();
	  objMessageBodyPart.setText("Please Find The Attached Report File!");
	  
	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(objMessageBodyPart); objMessageBodyPart = new
	  MimeBodyPart();
	  
	  //Set path to the pdf report file
	  
	  String filename =
	  System.getProperty("user.dir")+"\\test-output\\emailable-report.html";
	  //Create data source to attach the file in mail 
	  DataSource source = new
	  FileDataSource(filename);
	  
	  objMessageBodyPart.setDataHandler(new DataHandler(source));
	  objMessageBodyPart.setFileName(filename);
	  multipart.addBodyPart(objMessageBodyPart); message.setContent(multipart);
	  Transport transport = session.getTransport("smtp"); transport.connect(host,
	  from, pass); transport.sendMessage(message, message.getAllRecipients());
	  transport.close();
	  
	  }
	  
	  catch (AddressException ae) {
	  
	  ae.printStackTrace();
	  
	  }
	  
	  catch (MessagingException me) {
	  
	  me.printStackTrace();
	  
	  }*/
	  
	  }



  @Test(priority = 1 , enabled=true)
  public void checkBackoffice() throws InterruptedException {
	  
	  String homeTitle = "ConnectPay Business";
	  String agentName = "Priyanka";
	  driver.get(configFileReader.getBackofficeUrl());
      driver.manage().window().maximize();
      driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
      Thread.sleep(5000);
      AssertJUnit.assertEquals(driver.getTitle(),homeTitle);
      BackofcObj = new BackofficePage(driver);
      BackofcObj.EmailtxtBox.sendKeys(configFileReader.getUsername());
      BackofcObj.PasswordtxtBox.sendKeys(configFileReader.getbackofficePsw());
      BackofcObj.LoginBtn.click();
	  Thread.sleep(2000);
	  AssertJUnit.assertEquals(BackofcObj.agentName.getText(),agentName);
	  BackofcObj.LogoutBtn.click(); 
	  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
	}
  
  @Test(priority = 2 , enabled= true)
  public void checkOnboarding() throws InterruptedException {
	  
	  String homeTitle = "ConnectPay";
	  String headerText = "IBAN account for business";
	  String StepName = "Information";
  	  driver.get(configFileReader.getOnboardingUrl());
  	  driver.manage().window().maximize();
  	  driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
  	  Thread.sleep(5000);
  	  onboardingObj = new OnboardingPage(driver); 
  	  WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/app-root/div[1]/div/app-business/div/h4")));
  	  AssertJUnit.assertEquals(element.getText(),headerText);
  	  AssertJUnit.assertEquals(driver.getTitle(),homeTitle);
  	  onboardingObj.EmailtxtBox.sendKeys(configFileReader.getUsername());
  	  onboardingObj.AppRefTextbox.sendKeys(configFileReader.getAppRef());
  	  onboardingObj.ContinueBtn.click();
      WebElement StepInfo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()=\"Information\"]")));
      AssertJUnit.assertEquals(StepInfo.getText(),StepName);
    
   }
  
  @Test(priority = 3, enabled= true)
  public void checkCards() throws InterruptedException {
	
	  String homeTitle = "Connectpay Cards";
	  String headerText = "Cards application form";
	  String StepName = "Choose the IBAN account for which you want to order cards";
	  driver.get(configFileReader.getCardUrl());
  	  driver.manage().window().maximize();
  	  driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
  	  Thread.sleep(5000);
  	  WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/app-root/div[1]/div/app-login-process/div/h4")));
  	  AssertJUnit.assertEquals(element.getText(),headerText);
  	  AssertJUnit.assertEquals(driver.getTitle(),homeTitle);
      driver.findElement(By.xpath("//input[@name=\"userName\"]")).sendKeys(configFileReader.getUsername());
      driver.findElement(By.xpath("//input[@name=\"password\"]")).sendKeys(configFileReader.getcardsPsw());
      driver.findElement(By.xpath("//button[text()=\"Login \"]")).click();
      WebElement cardInfo = wait.until(ExpectedConditions.elementToBeClickable
            (By.xpath("/html/body/app-root/div[1]/div/app-login-process/div/div[3]/div/app-user-applications/h4")));
      AssertJUnit.assertEquals(cardInfo.getText(),StepName);
    
  }
  
  @Test(priority=4, enabled= true)
  public void checkMii() throws InterruptedException {
	
	  String homeTitle = "ConnectPay";
	  String headerText = "IBAN onboarding";
	  String StepName = "Phone number verification";
	  driver.get(configFileReader.getMiiUrl());
  	  driver.manage().window().maximize();
  	  driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
  	  WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/app-root/div[1]/app-home/div/app-login/div/h4")));
  	  Thread.sleep(5000);
  	  AssertJUnit.assertEquals(element.getText(),headerText);
  	  AssertJUnit.assertEquals(driver.getTitle(),homeTitle);
      driver.findElement(By.xpath("//input[@name=\"password\"]")).sendKeys(configFileReader.getmiiPsw());
      driver.findElement(By.xpath("//button[text()=\"Login \"]")).click();
      WebElement cardInfo = wait.until(ExpectedConditions.elementToBeClickable
    		(By.xpath("/html/body/app-root/div[1]/app-home/div/app-login/div/div[2]/div/app-otp-verification-pending/div/label")));
      AssertJUnit.assertEquals(cardInfo.getText(),StepName);
    
  }
}
