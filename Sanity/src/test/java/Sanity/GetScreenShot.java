package Sanity;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.google.common.io.Files;

public class GetScreenShot {
  @Test
  public static String capture(WebDriver driver) throws IOException
  {
	  DateFormat dateFormat = new SimpleDateFormat("MMddyyyyHHmmss");
	  Date date = new Date();
	  String date1= dateFormat.format(date);
	  String dest = System.getProperty("user.dir") +"\\Screenshots\\"+date1;
	  TakesScreenshot ts = (TakesScreenshot)driver;
	  File source = ts.getScreenshotAs(OutputType.FILE);
      File destination = new File(dest);
      Files.copy(source, destination);  
      return dest;
  }
}
