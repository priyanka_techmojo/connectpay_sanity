package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BackofficePage {
	WebDriver driver;

    @FindBy(id = "email")
	public WebElement EmailtxtBox;
    
    @FindBy(id = "password")
    public WebElement PasswordtxtBox;
    
    @FindBy(xpath = "//button[text()=\"Log in\"]")
    public WebElement LoginBtn;

    @FindBy(xpath = "//span[@id=\"backOfficeAgentName\"]")
    public WebElement agentName;
    
    @FindBy(xpath = "//a[text()=\"Logout\"]")
    public WebElement LogoutBtn;
    
    public BackofficePage(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }   

    

}
