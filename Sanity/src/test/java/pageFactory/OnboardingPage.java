package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OnboardingPage {
	WebDriver driver;

    @FindBy(xpath = "//div[@class=\"form-box right\"]/form/div/app-text-box//input[@id=\"ca-email\"]")
	public WebElement EmailtxtBox;
    
    @FindBy(xpath = "//div[@class=\"form-box right\"]/form/div/app-text-box//input[@id=\"ca-ref\"]")
    public WebElement AppRefTextbox;
    
    @FindBy(xpath = "//div[@class=\"form-box right\"]/form/button[text()=\"CONTINUE\"]")
    public WebElement ContinueBtn;

  
    
    public OnboardingPage(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }   

    

}
