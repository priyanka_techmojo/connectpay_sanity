package dataProvider;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
public class ConfigFileReader {

	 private Properties properties;
	 private final String propertyFilePath= "Config//Configurations.properties";
	 
	 
	 public ConfigFileReader(){
	 BufferedReader reader;
	 try {
	 reader = new BufferedReader(new FileReader(propertyFilePath));
	 properties = new Properties();
	 try {
	 properties.load(reader);
	 reader.close();
	 } catch (IOException e) {
	 e.printStackTrace();
	 }
	 } catch (FileNotFoundException e) {
	 e.printStackTrace();
	 throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
	 } 
	 }
	     public String getDriverPath(){
		 String driverPath = properties.getProperty("driverPath");
		 if(driverPath!= null) return driverPath;
		 else throw new RuntimeException("driverPath not specified in the Configurations.properties file."); 
		 }
	     
	     public String getReportPath(){
			 String reportPath = properties.getProperty("reportPath");
			 if(reportPath!= null) return reportPath;
			 else throw new RuntimeException("ReportPath not specified in the Configurations.properties file."); 
			 }
		 
		 public long getImplicitlyWait() { 
		 String implicitlyWait = properties.getProperty("implicitlyWait");
		 if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
		 else throw new RuntimeException("implicitlyWait not specified in the Configurations.properties file."); 
		 }
		 
		 public String getBackofficeUrl() {
		 String backofficeURL = properties.getProperty("backofficeURL");
		 if(backofficeURL != null) return backofficeURL;
		 else throw new RuntimeException("Backoffice url not specified in the Configurations.properties file.");
		 }
		 
		 public String getOnboardingUrl() {
			 String onboardingURL = properties.getProperty("onboardingURL");
			 if(onboardingURL != null) return onboardingURL;
			 else throw new RuntimeException("Onboarding url not specified in the Configurations.properties file.");
			 }
		 
		 public String getCardUrl() {
			 String cardURL = properties.getProperty("cardURL");
			 if(cardURL != null) return cardURL;
			 else throw new RuntimeException("Card url not specified in the Configurations.properties file.");
			 }
		 
		 public String getMiiUrl() {
			 String miiURL = properties.getProperty("miiURL");
			 if(miiURL != null) return miiURL;
			 else throw new RuntimeException("MII url not specified in the Configurations.properties file.");
			 }
		 
		 public String getUsername() {
			 String userName = properties.getProperty("userName");
			 if(userName != null) return userName;
			 else throw new RuntimeException("userName not specified in the Configurations.properties file.");
			 }
		 
		 public String getbackofficePsw() {
			 String backofficePsw = properties.getProperty("backofficePsw");
			 if(backofficePsw != null) return backofficePsw;
			 else throw new RuntimeException("backofficePsw not specified in the Configurations.properties file.");
			 }
		 
		 public String getmiiPsw() {
			 String miiPsw = properties.getProperty("miiPsw");
			 if(miiPsw != null) return miiPsw;
			 else throw new RuntimeException("miiPsw not specified in the Configurations.properties file.");
			 }
		 
		 public String getcardsPsw() {
			 String cardsPsw = properties.getProperty("cardsPsw");
			 if(cardsPsw != null) return cardsPsw;
			 else throw new RuntimeException("cardsPsw not specified in the Configurations.properties file.");
			 }
		 
		 public String getAppRef() {
			 String appReference = properties.getProperty("appReference");
			 if(appReference != null) return appReference;
			 else throw new RuntimeException("appReference not specified in the Configurations.properties file.");
			 }
}
